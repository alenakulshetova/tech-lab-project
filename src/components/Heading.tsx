import * as React from "react";

const style: React.CSSProperties = {
  padding: "20px 28px 0px 28px",
  marginBottom: "0px",
  textAlign: "center",
};

const Heading: React.FC<{ text: string }> = ({ text }) => {
  return <h3 style={style}>{text}</h3>;
};

export default Heading;
