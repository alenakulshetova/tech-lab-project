import * as React from "react";
import Button from "react-bootstrap/Button";
import Title from "../components/Title";
import pauseIcon from "../../assets/pause.svg";

const Timer: React.FC<{ minutes: string }> = ({ minutes }) => {
  enum TimerState {
    Initial,
    Running,
    Paused,
    TimesUp,
  }

  const [timerInSeconds, setTimerInSeconds] = React.useState(
    Number(minutes) * 60
  );

  const [timerState, setTimerState] = React.useState(TimerState.Initial);

  const timerIntervalRef = React.useRef<number>();

  const runTimer = React.useCallback(() => {
    timerIntervalRef.current = setInterval(() => {
      setTimerInSeconds(timerInSeconds - 1);
    }, 1000);
    setTimerState(TimerState.Running);
  }, [setTimerState, setTimerInSeconds, timerInSeconds]);

  const stopTimer = React.useCallback(() => {
    // stop timer
    clearInterval(timerIntervalRef.current);
    setTimerState(TimerState.Paused);
  }, [setTimerState]);

  React.useEffect(() => {
    runTimer();

    return () => clearInterval(timerIntervalRef.current);
  }, [runTimer]);

  const formatedTime = React.useMemo(() => {
    let minutes = Math.floor(timerInSeconds / 60);
    let second = timerInSeconds - minutes * 60;
    // let second = Math.floor(time % 60);

    return `${minutes}:${second < 10 ? "0" : ""}${second}`;
  }, [timerInSeconds]);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      {timerState == TimerState.TimesUp ? (
        <Title title="Time's up!"></Title>
      ) : (
        <>
          <Title
            title="Timer: "
            text={
              <span
                style={{
                  display: "inline-block",
                  textAlign: "right",
                  width: "45px",
                }}
              >
                {formatedTime}
              </span>
            }
          ></Title>
          <Button
            variant="outline-secondary"
            onClick={timerState === TimerState.Running ? stopTimer : runTimer}
            style={{
              width: "25px",
              height: "25px",
              padding: "0px",
              marginLeft: "7px",
            }}
          >
            <img
              src={pauseIcon}
              alt="pause"
              title="Bootstrap"
              style={{ width: "22px", height: "22px" }}
            />
          </Button>
        </>
      )}
    </div>
  );
};

export default Timer;
