import * as React from "react";

const style: React.CSSProperties = {
  lineHeight: "38px",
  margin: "0px 5px 0px 0px",
};

const Title: React.FC<{ text?: React.ReactNode; title: string }> = ({
  text,
  title,
}) => {
  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <h5 style={style}>{title}</h5>
      <p style={style}>{text}</p>
    </div>
  );
};

export default Title;
