import * as React from "react";
import { Link } from "react-navi";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import Form from "react-bootstrap/Form";
import icon from "../../assets/triangulo-3d-png-2.png";
import { Prompt } from "../pages/DashboardPage";

const SessionConfig: React.FC<{ prompts: Prompt[] }> = ({ prompts }) => {
  const [selectedPromptID, setSelectedPromptID] = React.useState();
  const handlePromptSelect = React.useCallback(
    (event: React.ChangeEvent<{ value: string }>) => {
      setSelectedPromptID(event.target.value);
    },
    [setSelectedPromptID]
  );

  const [selectedMinutes, setSelectedMinutes] = React.useState("5");
  const handleMinutesSelect = React.useCallback(
    (event: React.ChangeEvent<{ value: string }>) => {
      setSelectedMinutes(event.target.value);
    },
    [setSelectedMinutes]
  );

  console.log({ selectedPrompt: selectedPromptID, selectedMinutes });

  React.useEffect(() => {
    if (prompts.length > 0 && selectedPromptID == undefined) {
      setSelectedPromptID(prompts[0]._id);
    }
  }, [prompts, selectedPromptID]);

  return (
    <>
      <ListGroup style={{ margin: "15px 10px 35px 10px" }} variant="flush">
        <ListGroup.Item>
          <img
            src={icon}
            alt="icon"
            style={{ marginRight: 8, width: 32, height: 32 }}
          />
          Prompt
          <Form.Group controlId="exampleForm.SelectCustom">
            {/* <Form.Label>Select a topic</Form.Label> */}
            <Form.Control
              as="select"
              custom
              style={{ marginTop: 10 }}
              value={selectedPromptID}
              onChange={handlePromptSelect}
            >
              {prompts.map((prompt, index) => {
                return (
                  <option key={index} value={prompt._id}>
                    {prompt.title}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
        </ListGroup.Item>
        <ListGroup.Item>
          <img
            src={icon}
            alt="icon"
            style={{ marginRight: 8, width: 32, height: 32 }}
          />
          Timer {/* <Form.Label>Select a topic</Form.Label> */}
          <Form.Control
            as="select"
            custom
            style={{ marginTop: 10 }}
            value={selectedMinutes}
            onChange={handleMinutesSelect}
          >
            {[5, 6, 7, 8, 9, 10, 12, 15, 17, 20, 25, 30].map((minutes) => (
              <option key={minutes} value={minutes.toString()}>
                {minutes} minutes
              </option>
            ))}
          </Form.Control>
        </ListGroup.Item>
      </ListGroup>
      <Link href={`/session`}>
        <Button variant="outline-secondary" size="lg" block>
          Start your free writing session
        </Button>
      </Link>
    </>
  );
};

export default SessionConfig;
