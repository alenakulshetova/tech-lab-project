import * as React from "react";
import Button from "react-bootstrap/Button";

const style: React.CSSProperties = {
  marginLeft: "25px",
};

const MyButton = ({ text }) => {
  return (
    <Button variant="secondary" size="lg" block disabled>
      {text}
    </Button>
  );
};

export default MyButton;
