import * as React from "react";
import { Link } from "react-navi";
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";

const style: React.CSSProperties = {
  textAlign: "center",
};

const MainPageJumbotron = () => {
  return (
    <Jumbotron>
      <h1 style={style}>This is our web app!</h1>
      <p style={style}>
        This is a simple hero unit, a simple jumbotron-style component for
        calling extra attention to featured content or information.
      </p>
      <p style={style}>
        <Link href="/registration">
          <Button variant="success">Register</Button>
        </Link>
      </p>
    </Jumbotron>
  );
};

export default MainPageJumbotron;
