import * as React from "react";
import { Link, useNavigation } from "react-navi";
import localforage from "localforage";
import Navbar from "react-bootstrap/Navbar";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import icon from "../../assets/logo_icon.png";
import { getAPIMethodURL } from "../utils";

interface User {
  fullName: string;
  email: string;
}

const NavigationBar: React.FC<{}> = () => {
  const [user, setUser] = React.useState<User>();
  const isLoggedIn = user != undefined;
  const { navigate } = useNavigation();

  const fetchUser = React.useCallback(async () => {
    const token = await localforage.getItem("token");
    const fetchedUser = await fetch(getAPIMethodURL("user") + "/" + token, {
      method: "GET",
    });

    setUser((await fetchedUser.json()) as User);

    navigate("dashboard");
  }, [setUser]);

  React.useEffect(() => {
    fetchUser();
  }, []);

  const [email, setEmail] = React.useState("");
  const handleEmailChange = React.useCallback(
    (event: React.ChangeEvent<{ value: string }>) =>
      setEmail(event.target.value),
    [setEmail]
  );
  const [password, setPassword] = React.useState("");
  const handlePasswordChange = React.useCallback(
    (event: React.ChangeEvent<{ value: string }>) =>
      setPassword(event.target.value),
    [setPassword]
  );

  const handleLoginSubmit = React.useCallback(async () => {
    const tokenResponse = await fetch(getAPIMethodURL("user-auth"), {
      method: "POST",
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify({
        email,
        password,
      }),
    });

    const { token } = await tokenResponse.json();

    await localforage.setItem("token", token);

    await fetchUser();
  }, [email, password, fetchUser]);

  return (
    <Navbar bg="light" expand="lg">
      <img
        src={icon}
        alt="icon"
        style={{ marginRight: "10px", width: 50, height: 50 }}
      />
      <Link href="/">
        <Navbar.Brand>Free writing</Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse className="justify-content-end">
        {isLoggedIn ? (
          <>
            <OverlayTrigger
              trigger="click"
              placement="bottom"
              overlay={
                <Popover id={`popover-positioned-bottom`}>
                  <Popover.Content>
                    <Link href="/">
                      <Button variant="outline-secondary">Log out</Button>
                    </Link>
                  </Popover.Content>
                </Popover>
              }
            >
              <Button variant="outline-secondary">{user.fullName}</Button>
            </OverlayTrigger>{" "}
          </>
        ) : (
          <Form inline>
            <FormControl
              type="email"
              placeholder="Email"
              className="mr-sm-2"
              onChange={handleEmailChange}
            />
            <FormControl
              type="password"
              placeholder="Password"
              className="mr-sm-2"
              onChange={handlePasswordChange}
            />
            <Button onClick={handleLoginSubmit} variant="outline-secondary">
              Log in
            </Button>
          </Form>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavigationBar;
