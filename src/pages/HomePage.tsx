import * as React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NavigationBar from "../components/NavigationBar";
import MainPageCarousel from "../components/MainPageCarousel";
import MainPageJumbotron from "../components/MainPageJumbotron";

const HomePage: React.FC = () => {
  return (
    <div>
      <NavigationBar />
      <MainPageCarousel />
      <MainPageJumbotron />
    </div>
  );
};

export default HomePage;
