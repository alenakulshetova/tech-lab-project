import * as React from "react";
import { Link, useNavigation } from "react-navi";
import "bootstrap/dist/css/bootstrap.min.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import NavigationBar from "../components/NavigationBar";
import Heading from "../components/Heading";
import Title from "../components/Title";
import Timer from "../components/Timer";
import Button from "react-bootstrap/Button";
import { getAPIMethodURL } from "../utils";
import { Prompt } from "./DashboardPage";

const SessionPage: React.FC<{ minutes: string; promptID: string }> = ({
  minutes,
  promptID,
}) => {
  const [sessionText, setSessionText] = React.useState("");

  const handleTextChange = React.useCallback(
    (event) => {
      setSessionText(event.target.value);
    },
    [setSessionText]
  );

  const [prompt, setPrompt] = React.useState<Prompt>();

  const fetchPrompt = React.useCallback(async () => {
    const fetchedPrompt = await fetch(
      getAPIMethodURL("prompt") + "/" + promptID,
      {
        method: "GET",
      }
    );

    setPrompt((await fetchedPrompt.json()) as Prompt);
  }, [setPrompt]);

  React.useEffect(() => {
    fetchPrompt();
  }, []);

  const { navigate } = useNavigation();
  navigate("dashboard");

  return (
    <>
      <NavigationBar />
      <Container>
        <Row>
          <Col></Col>
          <Col xs={8}>
            <Heading text="Free writing session"></Heading>
            <Row style={{ margin: "10px 10px 0px 10px" }}>
              <Col sm={8}>
                <Title title="Prompt: " text={prompt.title}></Title>
              </Col>
              <Col
                sm={4}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-end",
                }}
              >
                <Timer minutes={minutes}></Timer>
              </Col>
            </Row>
            <InputGroup className="mb-3">
              <FormControl
                as="textarea"
                style={{ margin: "22px 0px 5px 0px", minHeight: "50vh" }}
                placeholder="Write here"
                aria-label="inputTextSession"
                aria-describedby="basic-addon1"
                value={sessionText}
                onChange={handleTextChange}
              />
            </InputGroup>
            <Row
              style={{
                margin: "0px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Link href="/dashboard">
                <Button
                  variant="outline-secondary"
                  size="lg"
                  style={{ width: "150px" }}
                >
                  Throw away
                </Button>
              </Link>
              <Link href="/dashboard">
                <Button
                  variant="outline-secondary"
                  size="lg"
                  style={{
                    width: "150px",
                  }}
                >
                  Save
                </Button>
              </Link>
            </Row>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </>
  );
};

export default SessionPage;
