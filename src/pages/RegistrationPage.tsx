import * as React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-navi";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import NavigationBar from "../components/NavigationBar";
import Heading from "../components/Heading";

const styles = {
  wraper: {
    position: "absolute",
    top: "0px",
    left: "0px",
    width: "100%",
    height: "100vh",
    backgroundColor: "hsla(176, 2%, 73%, 0.7)",
  },
  wraperSecond: {
    backgroundColor: "white",
    margin: "50px auto 0 auto",
    width: "70%",
    maxWidth: "570px",
  },
};

const RegistrationWindow: React.FC = () => {
  return (
    <>
      <NavigationBar />
      <Container>
        <Row>
          <Col></Col>
          <Col xs={8}>
            <Heading text="Registration"></Heading>
            <Form style={{ marginTop: "25px" }}>
              <Form.Group controlId="Name">
                <Form.Label>Full name</Form.Label>
                <Form.Control type="text" placeholder="Enter your full name" />
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <Link href="/dashboard">
                  <Button variant="outline-secondary" type="submit">
                    Submit
                  </Button>
                </Link>
              </div>
            </Form>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </>
  );
};

export default RegistrationWindow;
