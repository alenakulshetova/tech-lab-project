import * as React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-navi";
import NavigationBar from "../components/NavigationBar";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Heading from "../components/Heading";
import SessionConfig from "../components/SessionConfig";
import localforage from "localforage";
import { getAPIMethodURL } from "../utils";

export interface Prompt {
  _id: string;
  title: string;
}

const DashboardPage: React.FC = () => {
  const [prompts, setPrompts] = React.useState<Prompt[]>([]);

  const fetchPrompts = React.useCallback(async () => {
    const fetchedPrompts = await fetch(getAPIMethodURL("prompt-list"), {
      method: "GET",
    });

    setPrompts((await fetchedPrompts.json()) as Prompt[]);
  }, [setPrompts]);

  React.useEffect(() => {
    fetchPrompts();
  }, []);

  return (
    <>
      <NavigationBar />
      <Container>
        <Row>
          <Col></Col>
          <Col xs={8}>
            <Heading text="Persnal Dashboard"></Heading>
            <SessionConfig prompts={prompts}></SessionConfig>
          </Col>
          <Col></Col>
        </Row>
        <Row>
          <Card style={{ width: "18rem" }}>
            <Card.Body>
              <Card.Title>Date</Card.Title>
              <Card.Text>Prompt Writing time</Card.Text>
              <Link href="/viewsession/1/5">
                <Button variant="outline-secondary" size="lg" block>
                  View
                </Button>
              </Link>
            </Card.Body>
          </Card>
        </Row>
      </Container>
    </>
  );
};

export default DashboardPage;
