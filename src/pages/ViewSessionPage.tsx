import * as React from "react";
import { Link } from "react-navi";
import "bootstrap/dist/css/bootstrap.min.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import NavigationBar from "../components/NavigationBar";
import Heading from "../components/Heading";
import Title from "../components/Title";
import Timer from "../components/Timer";
import Button from "react-bootstrap/Button";
import prompts from "../data/prompts.json";

const ViewSessionPage: React.FC<{ minutes: string; promptID: number }> = ({
  minutes,
  promptID,
}) => {
  return (
    <>
      <NavigationBar />
      <Container>
        <Row>
          <Col></Col>
          <Col xs={8}>
            <Heading text="Free writing session"></Heading>
            <Row style={{ margin: "10px 10px 0px 10px" }}>
              <Col sm={8}>
                <Title title="Prompt: " text={prompts[promptID]}></Title>
              </Col>
              <Col
                sm={4}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-end",
                }}
              >
                <Title
                  title="Time: "
                  text={
                    <span
                      style={{
                        display: "inline-block",
                        textAlign: "right",
                        width: "45px",
                      }}
                    >
                      {minutes}
                    </span>
                  }
                ></Title>
              </Col>
            </Row>
            <InputGroup className="mb-3">
              <FormControl
                as="textarea"
                style={{ margin: "22px 0px 5px 0px", minHeight: "50vh" }}
                placeholder="Write here"
                aria-label="inputTextSession"
                aria-describedby="basic-addon1"
              />
            </InputGroup>
            <Row
            //   style={{
            //     margin: "0px",
            //   }}
            >
              <Link href={`/dashboard`}>
                <Button variant="outline-secondary" size="lg" block>
                  Exit
                </Button>
              </Link>
            </Row>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </>
  );
};

export default ViewSessionPage;
