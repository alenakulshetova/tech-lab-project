import * as React from "react";
import { Suspense } from "react";
import ReactDOM from "react-dom";
import { mount, route, lazy } from "navi";
import { Router, View } from "react-navi";
import "bootstrap/dist/css/bootstrap.min.css";
import HomePage from "./pages/HomePage";
import DashboardPage from "./pages/DashboardPage";
import SessionPage from "./pages/SessionPage";
import ViewSessionPage from "./pages/ViewSessionPage";
import RegistrationPage from "./pages/RegistrationPage";
import prompts from "data/prompts.json";

// Define routes using mount(), route(), and other middleware.
const routes = mount({
  "/": route({
    view: <HomePage />,
  }),
  "/registration": route({
    view: <RegistrationPage />,
  }),
  "/dashboard": route({
    view: <DashboardPage />,
  }),
  "/viewsession/:prompt/:minutes": route(async (req) => {
    let minutes = req.params.minutes;
    let promptID = req.params.prompt;

    return {
      view: <ViewSessionPage minutes={minutes} promptID={Number(promptID)} />,
    };
  }),

  "/session/:promptID/:minutes": route(async (req) => {
    let minutes = req.params.minutes;
    let promptID = req.params.promptID;

    return {
      view: <SessionPage minutes={minutes} promptID={promptID} />,
    };
  }),
});

// Then pass your routes to a `<Router>`, and render them with `<View>`.

function App() {
  return (
    <Router
      // Uncomment for deploy on pages
      //   basename={"tech-lab-project"}
      routes={routes}
    >
      <Suspense fallback={null}>
        <View />
      </Suspense>
    </Router>
  );
}

ReactDOM.render(<App />, document.getElementById("App"));
